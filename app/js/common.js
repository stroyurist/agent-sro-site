$(document).ready(function () {
    if ($('.owl-carousel').length) {

    $('.owl-carousel').owlCarousel({
        stagePadding: 200,
        loop: true,
        margin: 0,
        items: 1,
        lazyLoad: true,
        nav: false,
        dots: true,
        responsive: {
            0: {
                items: 1,
                stagePadding: 30
            },
            600: {
                items: 1,
                stagePadding: 100
            },
            1000: {
                items: 1,
                stagePadding: 200
            },
            1200: {
                items: 1,
                stagePadding: 250
            },
            1400: {
                items: 1,
                stagePadding: 300
            },
            1600: {
                items: 1,
                stagePadding: 350
            },
            1800: {
                items: 1,
                stagePadding: 470
            }
        }
    });
    $(".carousel__arrow_right").on ("click", function () {
        $('.owl-carousel').trigger('next.owl.carousel'); 
    });
}

    $('input[type="tel"]').inputmask({ "mask": "+7 ( 999 ) 999-99-99" });
});
